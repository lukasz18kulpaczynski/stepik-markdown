# Przykładowy Nagłówek README

## Paragraf Pierwszy
To jest pierwszy paragraf tekstu.

## Paragraf Drugi
To jest drugi paragraf. **tekst pogrubiony**, _tekst kursywą_, ~~tekst przekreślony~~.

## Paragraf Trzeci
To jest trzeci paragraf. Poniżej znajduje się przykład cytatu:

> Tutaj jest jakis cytat. 

## Lista Numeryczna

1. Pierwszy punkt
   1. Podpunkt pierwszego punktu
   2. Inny podpunkt
2. Drugi punkt
3. Trzeci punkt

## Lista Nienumeryczna

- Pierwszy punkt
  - Podpunkt pierwszego punktu
  - podpunkt
- Drugi punkt
- Trzeci punkt

## Jakis blok Kodu

```python
liczba = int(input("Podaj liczbę: "))
if liczba %2 == 0:
    print("Podana liczba jest podzielna przez 2")
else:
    print("Podana liczba nie jest podzielna przez 2")
```

## Jakies zdjecie 

![sponge.png](sponge.png)

